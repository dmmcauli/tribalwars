﻿<?php
// Einstellungen für das DS-Lan Ingame Forum



////////////////////////
// *******************//
// Fehlermeldungen    //
// *******************//
////////////////////////

// keine SF/SG Rechte
$status_error_right = 'You have no right to the founder of the tribe!';
// anderes Ally Forum
$status_error_ally = 'You do not have access to these forums!';
// keine Rechte zum Bearbeiten des Beitrages
$status_error_message = 'You have no right to edit!';
// Kein Bereich verfügbar
$status_error_area = 'The forum has no open topic!';
// Kein Thread verfügbar
$status_error_thread = 'No topic was created!';
// Kein Thread Error
$status_error_thread_zero = 'Topic gol!';
// Keine Ally verfügbar
$status_error_ally0 = 'Error!';
// Kein Thread zugriff
$status_error_thread_right = 'No rights for topic!';






///////////////////////
// ******************//
// Sprachdatei       //
// ******************//
///////////////////////

// ******************
// Allgemein
// ******************

$fgeneral_on = 'in data de ';
$fgeneral_to = ' la ora ';
$fgeneral_oclock = '';
// Forum Administrieren
$fgeneral_forumadmin = 'Administrare forum';

// ******************
// Overview
// ******************

// Neues Thema
$foverview_newthread = 'New topic';
// Themen
$foverview_thread = 'Topics';
// Autor
$foverview_author = 'Author';
// Letzter Post
$foverview_lastpost = '	Last theme';
// Antworten
$foverview_answer = 'Reply';

// ******************
//Thread
// ******************

// Nachricht Bearbeiten
$fthread_edit = 'Processing';
// Nachricht Löschen
$fthread_delete = 'Remove';
// NAchricht sicher löschen?
$fthread_delete_sure_m = '\'Do you really want to delete this post?\'';
// Theame wirklich löschen?
$fthread_delete_sure_t = '\'You really want to delete this topic?\'';
// Seite
$fthread_page = 'Pages: ';
// Antworten
$fthread_answer = 'Reply';
// Thread öffnen
$fthread_open = 'Open';
// Thread schliessen
$fthread_close = 'Close';
// Thread geschliessen
$fthread_closed = '(Closed)';
//
$fthread_admin_write = 'Locked.';


// ******************
//Thread answer, Thread edit 
// ******************

// Fett
$fbbcode_bold = 'Bold';
// Kursiv
$fbbcode_italic = 'Italics';
// Unterstrichen
$fbbcode_underline = 'Underline';
// Durchgestrichen
$fbbcode_strikethrought = 'Strikethrough';
// Zitat
$fbbcode_quote = 'Quote';
// URL Link
$fbbcode_url = 'Link';
// Spieler Verlinkung
$fbbcode_player = 'Player';
// Stammesverlinkung
$fbbcode_ally = 'Tribe';
// Koordinate
$ffcode_coord = 'Coordinate';
// Sende Button
$fsend_button = 'Send';

// ******************
// Admincenter
// ******************

// Forum 
$fadmin_forum = 'Forum';
// Sichtbarkeit
$fadmin_visibility = 'Visibility';
// Reihenfolge
$fadmin_order = 'To';
// Aktion
$fadmin_action = 'Action';
// Umnennen
$fadmin_nominal = 'Rename';
// Für alle Sichtbar
$fadmin_forall = 'For All';
// Forum hochschieben
$fadmin_up = 'Their';
// Forum runterschieben
$fadmin_down = 'If';
// Forum löschen
$fadmin_delete = 'Delete';
// Neues Forum erstellen
$fadmin_newforum = 'New Forum';
// Forenname
$fadmin_forumname = 'Forum Name: ';
// Sichbarkeit für neues Forum
$fadmin_newforall = 'Visible for Everyone';
// neues Forum erstellen
$fadmin_create = 'Create';

?>