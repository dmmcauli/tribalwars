<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sid Wrong - Empire of War</title>
	<link rel="icon" href="graphic/icons/rabe.png" type="image/x-icon"> 
	<script type="text/javascript" src="js/jquery.js?{$now}"></script>
	<script type="text/javascript" src="js/game.js?{$now}"></script>
	<link rel="stylesheet" href="css/game.css?{$now}" type="text/css" />
</head>

<body >
<table class="main" width="100%" align="center"><tr><td><h2>Session Timed Out</h2>
<p>Don't worry, this is just a bug where it doesn't work the first time. Login again: <a href="../index.php">Startseite</a>.</p>

</td></tr></table>
<script type="text/javascript">setImageTitles();</script>
</body>
</html>