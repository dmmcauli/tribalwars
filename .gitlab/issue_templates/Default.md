## Issue Template

**Reported by**: <reporter>

**Repro Steps**:
1. <repro step 1>
2. <repro step 2>

**Description**:
<description>

---

### Additional Information
- **Browser**: 
- **Commit**: *(if applicable)*
- **Screenshots**: *(if applicable)*

### Checklist
- [ ] Reproduction steps
- [ ] Labels